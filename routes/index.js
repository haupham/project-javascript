var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('Khach_hang/index');
});
router.get('/android', function(req, res, next) {
  res.render('Khach_hang/android');
});
router.get('/iphone', function(req, res, next) {
  res.render('Khach_hang/iphone');
});
router.get('/all', function(req, res, next) {
  res.render('Khach_hang/all');
});
router.get('/lien_he', function(req, res, next) {
  res.render('Khach_hang/lien_he');
});
router.get('/gio_hang', function(req, res, next) {
  res.render('Khach_hang/gio_hang');
});

module.exports = router;
