var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('Quan_Ly/index');
});
router.get('/Ung_dung_cho_Quan_ly', (req, res, next) => {
  res.render('Quan_Ly/Ung_dung_cho_Quan_ly')
});
router.get('/Them_dien_thoai', (req, res, next) => {
  res.render('Quan_Ly/Them_dien_thoai')
})
router.get('/MH_Loi_He_Thong', (req, res, next) => {
  res.render('MH_Loi_He_Thong')
})
router.get('/Sua_Dien_thoai', (req, res, next) => {
  res.render('Quan_Ly/Sua_Dien_thoai')
})
router.get('/Xoa_Thanh_ly_dien_thoai', (req, res, next) => {
  res.render('Quan_Ly/Xoa_Thanh_ly_dien_thoai')
})
router.get('/Phuc_hoi_DT_Da_xoa', (req, res, next) => {
  res.render('Quan_Ly/Phuc_hoi_DT_Da_xoa')
})
router.get('/Tim_kiem', (req, res, next) => {
  res.render('Quan_Ly/Tim_kiem')
})
router.get('/bieu_do_doanh_thu', (req, res, next) => {
  res.render('Quan_Ly/Bieu_do_Doanh_thu')
})
router.get('/Bieu_do_Phieu_ban', (req, res, next) => {
  res.render('Quan_Ly/Bieu_do_Phieu_ban')
})
router.get('/Bieu_do_Phieu_nhap', (req, res, next) => {
  res.render('Quan_Ly/Bieu_do_Phieu_nhap')
})
module.exports = router;
