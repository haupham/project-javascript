var XL_MONGO = require("./XL_Luu_tru_mongo.js");

class XL_LUU_TRU {
    async Doc_Thong_tin_Cua_hang() {
        var ThongTin = {}

        var Promise_Doi_tuong = XL_MONGO.Doc_danh_sach("Cong_ty")

        return Promise_Doi_tuong.then((Doi_tuong) => {
            Doi_tuong = Doi_tuong[0]
            ThongTin.Cua_hang = {}
            ThongTin.Cua_hang.Ten = Doi_tuong.Ten
            ThongTin.Cua_hang.Ma_so = Doi_tuong.Ma_so
            ThongTin.Cua_hang.Dien_thoai = Doi_tuong.Dien_thoai
            ThongTin.Cua_hang.Dia_chi = Doi_tuong.Dia_chi
            ThongTin.Cua_hang.Mail = Doi_tuong.Mail
            ThongTin.Cua_hang.footer = Doi_tuong.footer

            ThongTin.Danh_sach_Nhom_Dien_thoai = Doi_tuong.Danh_sach_Nhom_Dien_thoai

            ThongTin.Quan_ly = Doi_tuong.Quan_ly

            ThongTin.Nhan_vien_Nhap_hang = Doi_tuong.Nhan_vien_Nhap_hang

            ThongTin.Danh_sach_Nhan_vien_Ban_hang = Doi_tuong.Danh_sach_Nhan_vien_Ban_hang

            ThongTin.Danh_sach_Nhan_vien_Giao_hang = Doi_tuong.Danh_sach_Nhan_vien_Giao_hang

            return ThongTin
        });
    }

    async Doc_Danh_sach(collection) {
        var Danh_sach = []
        var Promise_Doi_tuong = XL_MONGO.Doc_danh_sach(collection)
        return Promise_Doi_tuong.then((Doi_tuong) => {
            Danh_sach = Doi_tuong
            return Danh_sach
        })
    }

    async Ghi_moi_Doi_tuong(collection, Doi_tuong) {
        var Promise_Doi_tuong = XL_MONGO.Them(collection, Doi_tuong)
        return Promise_Doi_tuong.then(() => {
            return "";
        }).catch((Loi) => {
            return Loi;
        })
    }

    async Cap_nhat_Doi_tuong(collection, Doi_tuong) {
        var dk = {}
        dk.Ma_so = Doi_tuong.Ma_so
        var update_v = {}
        update_v = Object.assign({}, Doi_tuong)

        delete update_v._id

        var Promise_Doi_tuong = XL_MONGO.CapNhat(collection, dk, update_v)
        return Promise_Doi_tuong.then(() => {
            return "";
        }).catch((Loi) => {
            return Loi;
        })
    }

    async Xoa_TL_Doi_tuong(collection, collection2, Doi_tuong) {
        var dk = {}
        dk.Ma_so = Doi_tuong.Ma_so

        var obj = Doi_tuong
        delete obj._id
        this.Ghi_moi_Doi_tuong(collection2, obj)

        var Promise_Doi_tuong = XL_MONGO.Xoa(collection, dk)
        return Promise_Doi_tuong.then(() => {
            return "";
        }).catch((Loi) => {
            return Loi;
        })
    }

    /*
     * collection2: nơi cần chuyển đến
     * collection: nơi chứa đối tượng
     */
    async Phuc_hoi_Doi_tuong(collection, collection2, Doi_tuong) {
        var dk = {}
        dk.Ma_so = Doi_tuong.Ma_so

        var obj = Doi_tuong
        delete obj._id
        this.Ghi_moi_Doi_tuong(collection2, obj)

        var Promise_Doi_tuong = XL_MONGO.Xoa(collection, dk)
        return Promise_Doi_tuong.then(() => {
            return "";
        }).catch((Loi) => {
            return Loi;
        })
    }
}
var Luu_tru = new XL_LUU_TRU()
module.exports = Luu_tru