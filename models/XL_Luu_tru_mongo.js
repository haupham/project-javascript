var DbConnection = require('./dbConnection.js');
var ObjectId = require('mongodb').ObjectID;

exports.tao_id = function (id) {
    var object_id = ObjectId(id);
    return object_id;
}

exports.Doc_danh_sach = async function(collection) {
    try {
        let db = await DbConnection.Get();
        let result = await db.collection(collection).find({}).toArray();
        return result;
    } catch (e) {
        console.log(e);
    }
}

exports.Them = async function(collection, thong_tin_doi_tuong){
    try {
        let db = await DbConnection.Get();
  
        let result = await db.collection(collection).insert(thong_tin_doi_tuong);
  
        return result;
       
    } catch (e) {
        console.log(e);
    }
}

/*
 * Bieu thuc dieu kien:
 * ex: { name: "Andy" }
 */
exports.CapNhat = async function(collection, bieu_thuc_dieu_kien, thong_tin_doi_tuong){
    try {
        let db = await DbConnection.Get();
  
        let result = await db.collection(collection).update(bieu_thuc_dieu_kien, thong_tin_doi_tuong);
        //console.log(result);
        return result;
       
    } catch (e) {
        console.log(e);
    }
}

exports.Xoa = async function(collection, bieu_thuc_dieu_kien){
    try {
        let db = await DbConnection.Get();
  
        let result = await db.collection(collection).remove(bieu_thuc_dieu_kien);
  
        return result;
       
    } catch (e) {
        console.log(e);
    }
}

// var File = require("fs")
// var Thu_muc_Du_lieu = "Dich_vu/Du_lieu"
// var Cong_nghe = "json"
// class XL_LUU_TRU {
//     Doc_Thong_tin_Cua_hang(){
//         var ThongTin = {}
//         var Duong_dan = Thu_muc_Du_lieu + "//Cong_ty//Cong_ty.json"
//         var Doi_tuong = File.readFileSync(Duong_dan, "UTF-8")
//         var Doi_tuong = JSON.parse(Doi_tuong)

//         ThongTin.Cua_hang = {}
//         ThongTin.Cua_hang.Ten = Doi_tuong.Ten
//         ThongTin.Cua_hang.Ma_so = Doi_tuong.Ma_so
//         ThongTin.Cua_hang.Dien_thoai = Doi_tuong.Dien_thoai
//         ThongTin.Cua_hang.Dia_chi = Doi_tuong.Dia_chi
//         ThongTin.Cua_hang.Mail = Doi_tuong.Mail
//         ThongTin.Cua_hang.footer = Doi_tuong.footer
        
//         ThongTin.Danh_sach_Nhom_Dien_thoai = Doi_tuong.Danh_sach_Nhom_Dien_thoai

//         ThongTin.Quan_ly = Doi_tuong.Quan_ly

//         ThongTin.Nhan_vien_Nhap_hang = Doi_tuong.Nhan_vien_Nhap_hang

//         ThongTin.Danh_sach_Nhan_vien_Ban_hang = Doi_tuong.Danh_sach_Nhan_vien_Ban_hang

//         ThongTin.Danh_sach_Nhan_vien_Giao_hang = Doi_tuong.Danh_sach_Nhan_vien_Giao_hang

//         return ThongTin
//     }

//     Doc_Danh_sach(Loai_Doi_tuong) {
//         var Danh_sach = []
//         var Duong_dan = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong
//         var Danh_sach_Ten_Tap_tin = File.readdirSync(Duong_dan, "UTF-8")
//         Danh_sach_Ten_Tap_tin.forEach(Ten => {
//             if (Ten.toLowerCase().endsWith(Cong_nghe)) {
//                 var Chuoi = File.readFileSync(Duong_dan + "//" + Ten, "UTF-8")
//                 var Doi_tuong = JSON.parse(Chuoi)
//                 Danh_sach.push(Doi_tuong)
//             }

//         })
//         return Danh_sach
//     }

//     Doc_Danh_sach_Xoa(Loai_Doi_tuong) {
//         var Danh_sach = []
//         var Duong_dan = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//Xoa"
//         var Danh_sach_Ten_Tap_tin = File.readdirSync(Duong_dan, "UTF-8")
//         Danh_sach_Ten_Tap_tin.forEach(Ten => {
//             if (Ten.toLowerCase().endsWith(Cong_nghe)) {
//                 var Chuoi = File.readFileSync(Duong_dan + "//" + Ten, "UTF-8")
//                 var Doi_tuong = JSON.parse(Chuoi)
//                 Danh_sach.push(Doi_tuong)
//             }

//         })
//         return Danh_sach
//     }

//     Doc_Danh_sach_Thanh_ly(Loai_Doi_tuong) {
//         var Danh_sach = []
//         var Duong_dan = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//Thanh_ly"
//         var Danh_sach_Ten_Tap_tin = File.readdirSync(Duong_dan, "UTF-8")
//         Danh_sach_Ten_Tap_tin.forEach(Ten => {
//             if (Ten.toLowerCase().endsWith(Cong_nghe)) {
//                 var Chuoi = File.readFileSync(Duong_dan + "//" + Ten, "UTF-8")
//                 var Doi_tuong = JSON.parse(Chuoi)
//                 Danh_sach.push(Doi_tuong)
//             }

//         })
//         return Danh_sach
//     }
//     Ghi_moi_Doi_tuong(Loai_Doi_tuong, Doi_tuong) {
//         var Kq = ""
//         try {
//             var Duong_dan = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//" + Doi_tuong.Ma_so + "." + Cong_nghe
//             var Chuoi = JSON.stringify(Doi_tuong)
//             File.writeFileSync(Duong_dan, Chuoi, "UTF-8")
//         } catch (Loi) {
//             Kq = Loi
//         }

//         return Kq
//     }
//     Cap_nhat_Doi_tuong(Loai_Doi_tuong, Doi_tuong) {
//         return this.Ghi_moi_Doi_tuong(Loai_Doi_tuong, Doi_tuong)
//     }
//     Thanh_ly_Doi_tuong(Loai_Doi_tuong, Doi_tuong) {
//         var Kq = ""
//         try {

//             var Duong_dan = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//" + Doi_tuong.Ma_so + "." + Cong_nghe
//             var Duong_dan_Thanh_ly = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//Thanh_ly//" + Doi_tuong.Ma_so + "." + Cong_nghe
//             File.unlinkSync(Duong_dan)
//             var Chuoi = JSON.stringify(Doi_tuong)
//             File.writeFileSync(Duong_dan_Thanh_ly, Chuoi, "UTF-8")
//         } catch (Loi) {
//             Kq = Loi
//         }

//         return Kq
//     }
//     Phuc_hoi_Doi_tuong(Loai_Doi_tuong, Doi_tuong) {
//         var Kq = ""
//         try {

//             var Duong_dan = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//" + Doi_tuong.Ma_so + "." + Cong_nghe
//             var Duong_dan_Thanh_ly = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//Xoa//" + Doi_tuong.Ma_so + "." + Cong_nghe
//             File.unlinkSync(Duong_dan_Thanh_ly)
//             var Chuoi = JSON.stringify(Doi_tuong)
//             File.writeFileSync(Duong_dan, Chuoi, "UTF-8")
//         } catch (Loi) {
//             Kq = Loi
//         }

//         return Kq
//     }
//     Xoa_Doi_tuong(Loai_Doi_tuong, Doi_tuong) {
//         var Kq = ""
//         try {
//             var Duong_dan = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//" + Doi_tuong.Ma_so + "." + Cong_nghe
//             var Duong_dan_Xoa = Thu_muc_Du_lieu + "//" + Loai_Doi_tuong + "//Xoa//" + Doi_tuong.Ma_so + "." + Cong_nghe
//             File.unlinkSync(Duong_dan)
//             var Chuoi = JSON.stringify(Doi_tuong)
//             File.writeFileSync(Duong_dan_Xoa, Chuoi, "UTF-8")

//         } catch (Loi) {
//             Kq = Loi
//         }

//         return Kq
//     }

// }
// var Luu_tru = new XL_LUU_TRU()
// module.exports = Luu_tru