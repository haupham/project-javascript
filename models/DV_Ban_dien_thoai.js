/*
 * Tên các collection của mongodb
 */
var DB_Dien_thoai = "Dien_thoai"
var DB_Xoa = "Xoa"
var DB_Thanh_ly = "Thanh_ly"

//====
var DB_Cong_ty = "Cong_ty"
//====

var http = require("http");
var port = 4000;
var File = require("fs");
var Xu_ly_Tham_so = require('querystring');
var Luu_tru = require("./XL_Luu_tru.js");
var Du_lieu_Ung_dung = {}
Khoi_dong_Du_lieu_Ung_dung().then((DATA) =>{
    Du_lieu_Ung_dung = DATA

    console.log("=======Du_lieu_ung_dung=======")
    console.log(DATA)
    console.log("=====end Du_lieu_ung_dung=====")
    
});
var DV_Ban_dien_thoai = http.createServer(
    (Yeu_cau, Dap_ung) => {

        var Chuoi_Nhan = ""
        var Chuoi_Kq = ""
        var Dia_chi = Yeu_cau
            .url
            .replace("/", "")
        var Tham_so = Xu_ly_Tham_so.parse(Dia_chi)
        var Loai_Doi_tuong = Tham_so.Loai_Doi_tuong
        var Ma_so_Xu_ly = Tham_so.Ma_so_Xu_ly
        var Ngay_Hien_hanh = new Date()
        Yeu_cau.on('data', (chunk) => { Chuoi_Nhan += chunk })
        Yeu_cau.on('end', () => {
            if (Ma_so_Xu_ly == "KHOI_DONG_DU_LIEU_QUAN_LY") {
                var Du_lieu = {}
                Du_lieu.Cua_hang = Du_lieu_Ung_dung.Cua_hang
                Du_lieu.Danh_sach_Dien_thoai = lay_thong_tin_Dien_thoai(Du_lieu_Ung_dung)
                Du_lieu.Danh_sach_Dien_thoai_Xoa = lay_thong_tin_Dien_thoai_xoa(Du_lieu_Ung_dung)
                Chuoi_Kq = JSON.stringify(Du_lieu)
            } else if (Ma_so_Xu_ly == "PHIEU_NHAP" || Ma_so_Xu_ly == "PHIEU_DAT" || Ma_so_Xu_ly == "PHIEU_BAN" || Ma_so_Xu_ly == "DOANH_THU") {
                var Doi_tuong = {}
                Doi_tuong.Danh_sach_Dien_thoai = []
                Du_lieu_Ung_dung.Danh_sach_Dien_thoai.forEach(Dien_thoai_Goc => {
                    var Dien_thoai = Object.assign({}, Dien_thoai_Goc)
                    Doi_tuong.Danh_sach_Dien_thoai.push(Dien_thoai)
                    if (Ma_so_Xu_ly != "PHIEU_BAN" && Ma_so_Xu_ly != "DOANH_THU")
                        delete Dien_thoai.Danh_sach_Phieu_Ban
                    if (Ma_so_Xu_ly != "PHIEU_DAT")
                        delete Dien_thoai.Danh_sach_Phieu_Dat
                    if (Ma_so_Xu_ly != "PHIEU_NHAP" && Ma_so_Xu_ly != "DOANH_THU")
                        delete Dien_thoai.Danh_sach_Phieu_Nhap
                })
                Doi_tuong.Danh_sach_Nhan_vien_Ban_hang = Du_lieu_Ung_dung.Danh_sach_Nhan_vien_Ban_hang
                Chuoi_Kq = JSON.stringify(Doi_tuong);
            } else if (Ma_so_Xu_ly == "DANG_NHAP_HE_THONG_QUAN_LY") {
                var Ten = Tham_so.Ten_Dang_nhap;
                var pwd = Tham_so.Mat_khau;
                if (Du_lieu_Ung_dung.Quan_ly.Ten_Dang_nhap == Ten
                    && Du_lieu_Ung_dung.Quan_ly.Mat_khau == pwd) {
                    var Doi_tuong = {
                        Ho_ten: `${Du_lieu_Ung_dung.Quan_ly.Ho_ten}`,
                        Ma_so: `${Du_lieu_Ung_dung.Quan_ly.Ma_so}`,
                        Ma_so_Loi: ''
                    }
                    Chuoi_Kq = JSON.stringify(Doi_tuong);
                } else {
                    var Dien_thoai = {
                        'Ma_so_Loi': "Đăng nhập bị lỗi"
                    }
                    Chuoi_Kq = JSON.stringify(Dien_thoai)
                }

            } else if (Ma_so_Xu_ly == "KHOI_DONG_DU_LIEU_KHACH_HANG") {
                var Du_lieu = {}
                Du_lieu.Cua_hang = Du_lieu_Ung_dung.Cua_hang
                Du_lieu.Danh_sach_Dien_thoai = lay_thong_tin_Dien_thoai(Du_lieu_Ung_dung)
                Chuoi_Kq = JSON.stringify(Du_lieu)

            } else if (Ma_so_Xu_ly == "GHI_MOI_DIEN_THOAI") { //ĐÃ EDIT
                var Dien_thoai = JSON.parse(Chuoi_Nhan)
                var Kq = ""
                Luu_tru.Ghi_moi_Doi_tuong( DB_Dien_thoai, Dien_thoai).then((data)=>{
                    Kq = data
                })
                
                if (Kq == "") {
                    Du_lieu_Ung_dung.Danh_sach_Dien_thoai.push(Dien_thoai)
                } else
                    Dien_thoai = {
                        'Ma_so_Loi': "LOI_CAP_NHAT"
                    }

                Chuoi_Kq = JSON.stringify(Dien_thoai)
            } else if (Ma_so_Xu_ly == "SUA_THONG_TIN_DIEN_THOAI") { //ĐÃ EDIT
                var Dien_thoai = JSON.parse(Chuoi_Nhan)
                var Kq = ""
                Luu_tru.Cap_nhat_Doi_tuong(DB_Dien_thoai, Dien_thoai).then((data) => {
                    Kq = data
                })

                if (Kq == "") {
                    Du_lieu_Ung_dung.Danh_sach_Dien_thoai.forEach(x => {
                        if (x.Ma_so == Dien_thoai.Ma_so) {
                            x.Ten = Dien_thoai.Ten
                            x.Don_gia_Ban = Dien_thoai.Don_gia_Ban
                        }
                    })
                } else
                    Dien_thoai = {
                        'Ma_so_Loi': "LOI_CAP_NHAT"
                    }

                Chuoi_Kq = JSON.stringify(Dien_thoai)
            } else if (Ma_so_Xu_ly == "XOA_DIEN_THOAI" || Ma_so_Xu_ly == "THANH_LY_DIEN_THOAI") {   //ĐÃ EDIT
                var Ma_so = Tham_so.Ma_so
                var Doi_tuong = Du_lieu_Ung_dung.Danh_sach_Dien_thoai.find(x => x.Ma_so == Ma_so)
                var Kq = ""
                if (Ma_so_Xu_ly == "XOA_DIEN_THOAI"){
                    Luu_tru.Xoa_TL_Doi_tuong(DB_Dien_thoai, DB_Xoa, Doi_tuong).then((data) => {
                        Kq = data
                    })
                }
                /* 
                    -----------------THANH_LY_DIEN_THOAI----------------
                */
                else{
                    Kq = Luu_tru.Xoa_TL_Doi_tuong(DB_Dien_thoai, DB_Thanh_ly, Doi_tuong).then((data) => {
                        Kq = data
                    })
                }
                /*
                    --------------END THANH_LY_DIEN_THOAI---------------
                */

                if (Kq == "") {
                    Du_lieu_Ung_dung.Danh_sach_Dien_thoai.forEach((dien_thoai, index) => {
                        if (dien_thoai.Ma_so == Doi_tuong.Ma_so) {
                            Du_lieu_Ung_dung.Danh_sach_Dien_thoai.splice(index, 1)
                            if (Ma_so_Xu_ly == "XOA_DIEN_THOAI")
                                Du_lieu_Ung_dung.Danh_sach_Dien_thoai_Xoa.push(Doi_tuong)
                            /* 
                                -----------------THANH_LY_DIEN_THOAI----------------
                            */
                            else Du_lieu_Ung_dung.Danh_sach_Dien_thoai_Thanh_ly.push(Doi_tuong)
                            /*
                                --------------END THANH_LY_DIEN_THOAI---------------
                            */
                        }
                    })
                    Doi_tuong = Du_lieu_Ung_dung
                } else
                    Doi_tuong = {
                        'Ma_so_Loi': "LOI_XOA_VA_THANH_LY_DIEN_THOAI"
                    }
                Chuoi_Kq = JSON.stringify(Doi_tuong)
            } else if (Ma_so_Xu_ly == "PHUC_HOI_DIEN_THOAI") {  //ĐÃ EDIT
                var Ma_so = Tham_so.Ma_so
                var Doi_tuong = Du_lieu_Ung_dung.Danh_sach_Dien_thoai_Xoa.find(x => x.Ma_so == Ma_so)
                var Kq = ""
                Luu_tru.Phuc_hoi_Doi_tuong(DB_Xoa, DB_Dien_thoai, Doi_tuong).then((data) => {
                    Kq = data
                })
                if (Kq == "") {
                    Du_lieu_Ung_dung.Danh_sach_Dien_thoai_Xoa.forEach((dien_thoai, index) => {
                        if (dien_thoai.Ma_so == Doi_tuong.Ma_so) {
                            Du_lieu_Ung_dung.Danh_sach_Dien_thoai_Xoa.splice(index, 1)
                            Du_lieu_Ung_dung.Danh_sach_Dien_thoai.push(Doi_tuong)
                        }
                    })
                    Doi_tuong = Du_lieu_Ung_dung
                } else
                    Doi_tuong = {
                        'Ma_so_Loi': "LOI_XOA_DIEN_THOAI"
                    }
                Chuoi_Kq = JSON.stringify(Doi_tuong)
            } else if (Ma_so_Xu_ly == "THONG_TIN_CUA_HANG") {   //ĐÃ EDIT
                var Thong_tin = {}
                Thong_tin.Cua_hang = Du_lieu_Ung_dung.Cua_hang
                Thong_tin.Quan_ly = Du_lieu_Ung_dung.Quan_ly
                Thong_tin.Nhan_vien = Du_lieu_Ung_dung.Nhan_vien
                // Thong_tin.Xoa = Du_lieu_Ung_dung.Danh_sach_Dien_thoai_Xoa.length
                Chuoi_Kq = JSON.stringify(Thong_tin)
            } else if (Ma_so_Xu_ly == "THEM_PHIEU_DAT_HANG") {
                var Dien_thoai = JSON.parse(Chuoi_Nhan)
                var Kq = Luu_tru.Cap_nhat_Doi_tuong("DIEN_THOAI", Dien_thoai)

                if (Kq == "") {
                    Du_lieu_Ung_dung.Danh_sach_Dien_thoai.forEach(x => {
                        if (x.Ma_so == Dien_thoai.Ma_so) {
                            x.Danh_sach_Phieu_Dat_hang = Dien_thoai.Danh_sach_Phieu_Dat_hang
                        }
                    })
                } else
                    Dien_thoai = {
                        'Ma_so_Loi': "LOI_THEM_PHIEU_DAT_HANG"
                    }

                Chuoi_Kq = JSON.stringify(Dien_thoai)
            }
            Dap_ung.setHeader("Access-Control-Allow-Origin", '*')
            Dap_ung.end(Chuoi_Kq);
        })

    })

DV_Ban_dien_thoai.listen(port, function () {
    console.log("Dịch vụ đang thực thi tại cổng: " + port)
});

async function Khoi_dong_Du_lieu_Ung_dung() {
    var Du_lieu = {}
    var TTCH = {}

    var PromiseTT_CH = Luu_tru.Doc_Thong_tin_Cua_hang()
    var PromiseDSDT = Luu_tru.Doc_Danh_sach(DB_Dien_thoai)
    var PromiseDSX = Luu_tru.Doc_Danh_sach(DB_Xoa)
    var PromiseDSTL = Luu_tru.Doc_Danh_sach(DB_Thanh_ly)

    return Promise.all([PromiseTT_CH, PromiseDSDT, PromiseDSX, PromiseDSTL]).then((v) => {
        Du_lieu.Danh_sach_Dien_thoai_Xoa = []
        Du_lieu.Danh_sach_Dien_thoai_Thanh_ly = []
        Du_lieu.Danh_sach_Dien_thoai = []

        TTCH = v[0]
        Du_lieu.Danh_sach_Dien_thoai = v[1]
        Du_lieu.Danh_sach_Dien_thoai_Xoa = v[2]
        Du_lieu.Danh_sach_Dien_thoai_Thanh_ly = v[3]

        Du_lieu.Cua_hang = TTCH.Cua_hang
        Du_lieu.Danh_sach_Nhom_Dien_thoai = TTCH.Danh_sach_Nhom_Dien_thoai
        Du_lieu.Quan_ly = TTCH.Quan_ly
        Du_lieu.Nhan_vien_Nhap_hang = TTCH.Nhan_vien_Nhap_hang
        Du_lieu.Danh_sach_Nhan_vien_Ban_hang = TTCH.Danh_sach_Nhan_vien_Ban_hang
        Du_lieu.Danh_sach_Nhan_vien_Giao_hang = TTCH.Danh_sach_Nhan_vien_Giao_hang

        return Du_lieu;
    })
}

function lay_thong_tin_Dien_thoai(Du_lieu_Ung_dung) {
    Danh_sach_Dien_thoai = []
    console.log(Danh_sach_Dien_thoai)
    Du_lieu_Ung_dung.Danh_sach_Dien_thoai.forEach(Dien_thoai_Goc => {
        var Dien_thoai = Object.assign({}, Dien_thoai_Goc)
        Danh_sach_Dien_thoai.push(Dien_thoai)
        delete Dien_thoai.Danh_sach_Phieu_Ban
        delete Dien_thoai.Danh_sach_Phieu_Dat
        delete Dien_thoai.Danh_sach_Phieu_Nhap
    })
    return Danh_sach_Dien_thoai
}

function lay_thong_tin_Dien_thoai_xoa(Du_lieu_Ung_dung) {
    Danh_sach_Dien_thoai = []
    Du_lieu_Ung_dung.Danh_sach_Dien_thoai_Xoa.forEach(Dien_thoai_Goc => {
        var Dien_thoai = Object.assign({}, Dien_thoai_Goc)
        Danh_sach_Dien_thoai.push(Dien_thoai)
        delete Dien_thoai.Danh_sach_Phieu_Ban
        delete Dien_thoai.Danh_sach_Phieu_Dat
        delete Dien_thoai.Danh_sach_Phieu_Nhap
    })
    return Danh_sach_Dien_thoai
}

module.exports = DV_Ban_dien_thoai;