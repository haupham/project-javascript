class XL_DU_LIEU {
    
    static Du_lieu_phieu_nhap(Ham_Xu_ly_Sau_khi_Khoi_dong){
        var Chuoi_Tham_so = "Ma_so_Xu_ly=PHIEU_NHAP"
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, "", (Du_lieu)=>{
            Ham_Xu_ly_Sau_khi_Khoi_dong(Du_lieu)
        })
    }

    static Du_lieu_phieu_ban(Ham_Xu_ly_Sau_khi_Khoi_dong){
        var Chuoi_Tham_so = "Ma_so_Xu_ly=PHIEU_BAN"
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, "", (Du_lieu)=>{
            Ham_Xu_ly_Sau_khi_Khoi_dong(Du_lieu)
        })
    }

    static Du_lieu_doanh_thu(Ham_Xu_ly_Sau_khi_Khoi_dong){
        var Chuoi_Tham_so = "Ma_so_Xu_ly=DOANH_THU"
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, "", (Du_lieu)=>{
            Ham_Xu_ly_Sau_khi_Khoi_dong(Du_lieu)
        })
    }
	
	static Dang_nhap_He_thong_Quan_ly(Ten_Dang_nhap,Mat_khau,Ham_Xu_ly_Sau_khi_Khoi_dong)
    {
        var Chuoi_Tham_so = `Ma_so_Xu_ly=DANG_NHAP_HE_THONG_QUAN_LY&Ten_Dang_nhap=${Ten_Dang_nhap}`
        Chuoi_Tham_so+=`&Mat_khau=${Mat_khau}`
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so,"", (Doi_tuong) => {
            // if (Doi_tuong.Ma_so_Loi) {
            //     localStorage.Ma_so_Loi = Doi_tuong.Ma_so_Loi
            //     location.href = "MH_Loi_He_thong.html"
            // }
            Ham_Xu_ly_Sau_khi_Khoi_dong(Doi_tuong)
        })
    }

    static Khoi_dong_Du_lieu(Ham_Xu_ly_Sau_khi_Khoi_dong) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=KHOI_DONG_DU_LIEU_QUAN_LY"
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, "", (Du_lieu) => {
            Ham_Xu_ly_Sau_khi_Khoi_dong(Du_lieu)
        })
    }

    static Ghi_moi_Dien_thoai(Dien_thoai) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=GHI_MOI_DIEN_THOAI"
        var Chuoi_Goi = JSON.stringify(Dien_thoai)
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, Chuoi_Goi, (Doi_tuong) => {
            if (Doi_tuong.Ma_so_Loi) {
                localStorage.Ma_so_Loi = Doi_tuong.Ma_so_Loi
                location.href = "quanly/MH_Loi_He_Thong"
            }else parent.location.href=parent.location.href
        })
    }

    static Sua_Thong_tin_Dien_thoai(Dien_thoai) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=SUA_THONG_TIN_DIEN_THOAI"
        var Chuoi_Goi = JSON.stringify(Dien_thoai)
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, Chuoi_Goi, (Doi_tuong) => {
            if (Doi_tuong.Ma_so_Loi) {
                localStorage.Ma_so_Loi = Doi_tuong.Ma_so_Loi
                location.href = "quanly/MH_Loi_He_Thong"
            }
        })
    }

    static Xoa_Dien_thoai(Ma_so) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=XOA_DIEN_THOAI&&Ma_so=" + Ma_so
        var Chuoi_Goi = ""
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, Chuoi_Goi, (Doi_tuong) => {
            if (Doi_tuong.Ma_so_Loi) {
                localStorage.Ma_so_Loi = Doi_tuong.Ma_so_Loi
                location.href = "quanly/MH_Loi_He_Thong"
            }
        })
    }

    static Thanh_ly_Dien_Thoai(Ma_so) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=THANH_LY_DIEN_THOAI&&Ma_so=" + Ma_so
        var Chuoi_Goi = ""
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, Chuoi_Goi, (Doi_tuong) => {
            if (Doi_tuong.Ma_so_Loi) {
                localStorage.Ma_so_Loi = Doi_tuong.Ma_so_Loi
                location.href = "quanly/MH_Loi_He_Thong"
            }
        })
    }

    static Phuc_hoi_Dien_thoai(Ma_so) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=PHUC_HOI_DIEN_THOAI&&Ma_so=" + Ma_so
        var Chuoi_Goi = ""
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, Chuoi_Goi, (Doi_tuong) => {
            if (Doi_tuong.Ma_so_Loi) {
                localStorage.Ma_so_Loi = Doi_tuong.Ma_so_Loi
                location.href = "quanly/MH_Loi_He_Thong"
            }else parent.location.href=parent.location.href
        })
    }

    static Thong_tin_Cua_hang(Ham_Xu_ly_Sau_khi_Khoi_dong) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=THONG_TIN_CUA_HANG"
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, "", (Du_lieu) => {
            Ham_Xu_ly_Sau_khi_Khoi_dong(Du_lieu)
        })
    }

    /*
        ================================ KHÁCH HÀNG ==================================    
    */
    static KHOI_DONG_DU_LIEU_KHACH_HANG(Ham_Xu_ly_Sau_khi_Khoi_dong) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=KHOI_DONG_DU_LIEU_KHACH_HANG"
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, "", (Du_lieu) => {
            Ham_Xu_ly_Sau_khi_Khoi_dong(Du_lieu)
        })
    }

    static THEM_PHIEU_DAT_HANG(Dien_thoai) {
        var Chuoi_Tham_so = "Ma_so_Xu_ly=THEM_PHIEU_DAT_HANG"
        var Chuoi_Goi = JSON.stringify(Dien_thoai)
        Kich_Hoat_Xu_ly_JSON(Chuoi_Tham_so, Chuoi_Goi, (Doi_tuong) => {
            if (Doi_tuong.Ma_so_Loi) {
                localStorage.Ma_so_Loi = Doi_tuong.Ma_so_Loi
                location.href = "MH_Loi_He_thong.html"
            }
        })
    }
    /*
        ============================= HẾT - KHÁCH HÀNG ===============================
    */

}

class XL_DIEN_THOAI {
    static Tao_The_hien(Dien_thoai, Th_cha) {
        var Th = document.createElement("tr")
        Th_cha.appendChild(Th)
        var noi_dung = ""
        noi_dung += `<td>${Dien_thoai.Ten}</td>`
        noi_dung += `<td>${Dien_thoai.Ma_so}</td>`
        noi_dung += `<td>${Tao_Chuoi_The_hien_So_nguyen_duong(Dien_thoai.Don_gia_Ban)}</td>`
        noi_dung += `<td>${Dien_thoai.Nhom_Dien_thoai.Ten}</td>`
        Th.innerHTML = noi_dung
        return Th
    }

    static Tao_The_hien_Xoa_Thanh_ly(Dien_thoai, Th_cha, Ten_nut) {
        var Th = document.createElement("tr")
        Th_cha.appendChild(Th)
        var noi_dung = ""
        noi_dung += `<td>${Dien_thoai.Ten}</td>`
        noi_dung += `<td>${Dien_thoai.Ma_so}</td>`
        noi_dung += `<td>${Tao_Chuoi_The_hien_So_nguyen_duong(Dien_thoai.Don_gia_Ban)}</td>`
        noi_dung += `<td>${Dien_thoai.Nhom_Dien_thoai.Ten}</td>`
        noi_dung += `<td><button class="btn btn-danger" value="${Dien_thoai.Ma_so}" onclick="ChucNang(this)">${Ten_nut}</button></td>`
        Th.innerHTML = noi_dung
        return Th
    }

    static Tao_The_hien_Xoa_Thanh_ly_2(Dien_thoai, Th_cha) {
        var Th = document.createElement("tr")
        Th_cha.appendChild(Th)
        var noi_dung = ""
        noi_dung += `<td>${Dien_thoai.Ten}</td>`
        noi_dung += `<td>${Dien_thoai.Ma_so}</td>`
        noi_dung += `<td>${Tao_Chuoi_The_hien_So_nguyen_duong(Dien_thoai.Don_gia_Ban)}</td>`
        noi_dung += `<td>${Dien_thoai.Nhom_Dien_thoai.Ten}</td>`
        noi_dung += `<td><input class="deleted_pro" type="checkbox" value="${Dien_thoai.Ma_so}"></td>`
        Th.innerHTML = noi_dung
        return Th
    }

    static Tao_the_hien_Them_Dien_thoai(Th_Cha) {
        var The_hien = document.createElement("table");
        Th_Cha.appendChild(The_hien);
        The_hien.className = "table table-bordered";
        var noi_dung = "";
        noi_dung += `<tr>`
        noi_dung += `<td>Tên</td><td><input type="text" id="Th_Ten" /></td>`
        noi_dung += `</tr>`
        noi_dung += `<tr>`
        noi_dung += `<td>Giá</td><td><input type="text" id="Th_Don_gia_Ban" /></td>`
        noi_dung += `</tr>`
        noi_dung += `<tr>`
        noi_dung += `<td>Nhóm</td><td><select id="Th_Nhom_Dien_thoai">`
        noi_dung += `<option value="ANDROID">ANDROID</option>`
        noi_dung += `<option value="IPHONE">IPHONE</option>`
        noi_dung += `</select></td>`
        noi_dung += `</tr>`
        The_hien.innerHTML = noi_dung
        return The_hien
    }

    /*
        ================================ KHÁCH HÀNG ==================================    
    */

    static Tao_the_hien_Khach_hang(Dien_thoai, Th_cha) {
        var Th = document.createElement("div")
        Th_cha.appendChild(Th)
        Th.className = "sp"
        Th.onclick = function () {
            if (Th.style.opacity == 1) {
                Th.style.opacity = 0.6;
                localStorage.setItem(Dien_thoai.Ma_so, Dien_thoai.Ma_so)
            }
            else {
                Th.style.opacity = 1;
                localStorage.removeItem(Dien_thoai.Ma_so)
            }
        }
        Th.style.opacity = 1;
        var noi_dung = ""
        noi_dung += `<img src="/images/Dien_thoai/${Dien_thoai.Ma_so}.png">`
        noi_dung += `<span>${Dien_thoai.Ten}</span></br>`
        noi_dung += `<span class="gia">${Tao_Chuoi_The_hien_So_nguyen_duong(Dien_thoai.Don_gia_Ban)}</span>`
        Th.innerHTML = noi_dung
        return Th
    }

    static Tao_the_hien_Gio_hang(Dien_thoai, Th_cha) {
        var Th = document.createElement("tr")
        Th_cha.appendChild(Th)
        Th.id = Dien_thoai.Ma_so
        var noi_dung = ""
        noi_dung += `<td><img src="../images/Dien_thoai/${Dien_thoai.Ma_so}.png"></td>`
        noi_dung += `<td>${Dien_thoai.Ten}</td>`
        noi_dung += `<td>${Tao_Chuoi_The_hien_So_nguyen_duong(Dien_thoai.Don_gia_Ban)}</td>`
        noi_dung += `<td><input type="number" value="1" min="1" class="SoLuong" Ma_so="${Dien_thoai.Ma_so}" style="text-align:right"></td>`
        noi_dung += `<td><input type="checkbox" value="${Dien_thoai.Ma_so}" class="SanPham"></td>`
        Th.innerHTML = noi_dung
        return Th
    }

    /*
        ============================= HẾT - KHÁCH HÀNG ===============================
    */
}