var init = 1
function setFontSize(){
	if ($(window).width()>900 && init!=1)
		return
	var i = 48*$(window).width()/750;
	var j = 80*$(window).width()/750;
	$("#Thong_tin").css({
		'font-size': i>57?57:i
	});
	$("#Th_Ten_CH").css({
		'font-size': j>80?80:j
	});
	init++
}

/*
	Tạo navbar
*/
var navbar = document.getElementById("navbar");
var content = document.getElementById("Noi_dung");
var padding_left_items_navbar = ($(window).width()-550)/2;
// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function navbar_f() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
    content.classList.add("sticky2")
   	$("#navbar a:nth-child(2), .x_close, #search, #input_search").css({
   		'position': 'fixed'
   	});
   	$("#input_search").css('top', 55);
  } else {
    navbar.classList.remove("sticky")
    content.classList.remove("sticky2")
   	$("#navbar a:nth-child(2), .x_close, #search, #input_search").css({
   		'position': 'absolute'
   	});
   	$("#input_search").css('top', "");
  }

  opacity_info()
}


/*
	Tạo navbar - END
*/

function opacity_info(){
	var wos = window.pageYOffset
	if (wos < sticky){
		var i = wos / (sticky + 300)
		$("#Thong_tin div").css({
			'opacity': 1 - i
		});
	}
}

var navbar_height = $("#navbar a").length * 53;

function trans_icon_x(e){
	e.classList.toggle("change")

	// var d = document.getElementById("navbar")
	// if (d.classList.contains('responsive'))
	// 	d.classList.remove('responsive')
	// else
	// 	d.classList.add('responsive')
	if (e.classList.contains('change'))
		$("#navbar").css('height', navbar_height);
	else
		$("#navbar").css('height', 53);
}

function click_search(){
	$("#input_search").toggle("hidden")
}

function even_target(even){

}

window.onload = setFontSize;
window.onresize = setFontSize;
window.onscroll = navbar_f;